import { Card, Button } from "react-bootstrap";
import { useState } from "react";

export default function CourseCard({ courseProps }) {
   // Check to see if the data is successfullly passed
   console.log(courseProps);
   console.log(typeof courseProps);

   // destructuring the data to avoid dot notation

   const { name, description, price } = courseProps;

   // 3 hooks in React
   // 1. useState
   // 2. useEffect
   // 3. useContext

   // Use the useState hook for the component to be able to store state
   // State are used to keep track of information related to individual components
   // Syntax -> const [getter, setter] = useState(initialGetterValue)

   const [count, setCount] = useState(0);
   const [seat, seatCount] = useState(30);

   function enroll() {
      if (seat <= 0) {
         alert("No more seats");
      } else {
         setCount(count + 1);
         seatCount(seat - 1);
      }
   }

   return (
      <>
         <Card className="m-3 my-3">
            <Card.Body>
               <Card.Title>
                  <h5>{name}</h5>
               </Card.Title>
               <Card.Text>
                  <h6 className="">Description:</h6>
                  <p className="pb-2">{description}</p>
                  <h6 className="">Price:</h6>
                  <p className="pb-2">{price}</p>
               </Card.Text>
               <Card.Text>
                  Enrolles: {count} Seat: {seat}
               </Card.Text>
               <Button className="btnEnroll" onClick={enroll} variant="primary">
                  Enroll
               </Button>
            </Card.Body>
         </Card>
      </>
   );
}
