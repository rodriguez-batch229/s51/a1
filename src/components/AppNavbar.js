import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

export default function AppNavbar() {
   return (
      <Navbar expand="md" className="text-light p-2 m-1">
         <Navbar.Brand href="#home">Zuitt</Navbar.Brand>

         <Navbar.Toggle aria-controls="basic-navbar-nav" />
         <Navbar.Collapse id="basic-navbar-bav">
            <Nav className="ml-auto">
               <Nav.Link href="#home">Home</Nav.Link>
               <Nav.Link href="#courses">Courses</Nav.Link>
            </Nav>
         </Navbar.Collapse>
      </Navbar>
   );
}
