import "./App.css";
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js";
import Courses from "./pages/Courses.js";
import { Container } from "react-bootstrap";

function App() {
   return (
      // Fragment: <> and </>
      /* Mount component and prepare output rendering */
      <div>
         <AppNavbar />
         <Container>
            <Home />
            <Courses />
         </Container>
      </div>
   );
}

export default App;
